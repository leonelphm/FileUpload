Para instalar el demo en modo desarrollo debera seguir los siguientes pasos:

1-) Instalar el controlador de versiones git:
---------------------------------------------
    
    $ su

    # aptitude install git

2-) Descargar el codigo fuente del demo
---------------------------------------

    Para descargar el código fuente del demo contenido en su repositorio GIT realice un clon del demo FileUpload, con el siguiente comando:

    $ git clone https://gitlab.com/leonelphm/FileUpload.git
                

3-) Preparar el Backend (FileUpload):
-------------------------------------

Entrar en la carpeta 'FileUpload' y seguir los siguientes pasos:


3.1-) Crear un Ambiente Virtual:
--------------------------------
 El demo está desarrollado con el lenguaje de programación Python, se debe instalar Python >= v3.5.x. Con los siguientes comandos puede instalar Python y PIP.

    Entrar como root para la instalacion 

    # aptitude install python3.5 python3-pip python3.4-dev python3-setuptools

    # aptitude install python3-virtualenv virtualenvwrapper

    Salir del modo root y crear el ambiente:

    $ mkvirtualenv --python=/usr/bin/python3 file-upload

3.2-) Instalar los requerimientos del demo:
-------------------------------------------

    Para activar el ambiente virtual file-upload ejecute el siguiente comando:

    $ workon file-upload

    (file-upload)$

    Entrar en la carpeta raiz del demo:

    (file-upload)$ cd file-upload

    (file-upload)file-upload$ 

    Desde ahi se deben instalar los requerimientos del proyecto con el siguiente comando:

    (file-upload)$ pip install -r requerimientos.txt

    De esta manera se instalaran todos los requerimientos iniciales para montar el demo 


3.3-) Migrar los modelos:
-----------------------------------------------

    El manejador de base de datos que usa el demo es sqlite3.

    Para migrar los modelos del demo se debe usar el siguiente comando:

    (file-upload)$ python manage.py makemigrations user
    (file-upload)$ python manage.py migrate

4-) Iniciar el demo:
------------------------

    (file-upload)$ python manage.py runserver


5-) Preparar el frontend (frontend-file):
----------------------------------------

Se debe ingresar al directorio 'frontend-file' y seguir los pasos que estan descritos en el README.md



Rutas de la api:

	http://localhost:8000/api/archivo/upload/<str:nombre_archivo>

Ruta por defecto del frontend desarrollado con vue:

	http//localhost:8080/