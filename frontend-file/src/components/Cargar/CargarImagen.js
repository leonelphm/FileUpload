import axios from 'axios';
import ApiRoute from '@/ApiRoute.js'

export default {
    name: 'CargarImagen',
    data () {
        return {
            dialog: false,
            imgNombre: '',
            imgArchivo: '',
            imgUrl: '',
            imgTitulo: "Cargar Imagen"
        }
    },
    methods: {
        obtenerArchivo () {
            this.$refs.image.click ()
        },
        
        valoresArchivos (event) {
            const archivo = event.target.files
            if(archivo[0] !== undefined) {
                this.imgNombre = archivo[0].name
                const lecturaArchivo = new FileReader ()
                lecturaArchivo.readAsDataURL(archivo[0])
                lecturaArchivo.addEventListener('load', () => {
                    this.imgUrl = lecturaArchivo.result
                    this.imgArchivo = archivo[0]
                })
            } else {
                this.imgNombre = ''
                this.imgArchivo = ''
                this.imgUrl = ''
            }
        },
        cargar(){
            if (this.imgArchivo){
                axios
                  .put(ApiRoute.CargarArchivoUrl()+this.imgNombre+"/", this.imgArchivo)
                  .then(response => {
                    if (response.status == 201){
                        this.$notify({
                          title: 'Éxito',
                          text: "Se cargo la imagen al servidor",
                          type: 'success',
                        });
                    }
                  })
                  .catch(error => {
                    this.$notify({
                      title: 'Error',
                      text: error,
                      type: 'error',
                    });
                  });
            }
            else{
                this.$notify({
                  title: 'Error',
                  text: "No selecciono una imagen",
                  type: 'error',
                });
            }
        }
    }
}
