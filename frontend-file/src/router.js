import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import CargarImagen from './components/Cargar/CargarImagen.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'inicio',
      component: App
    },
    {
      path: '/carga-imagen/',
      name: 'carga_imagen',
      component: CargarImagen
    },
  ]
})