"""
FileUpload Demo
"""

## @package user.urls
#
# Rutas de cominicacion entre las vistas y las plantillas
# @author Ing. Leonel P. Hernandez M.
# @date 01-11-2017
# @version 1.0

from django.urls import path

from .views import CargarArchivoView

urlpatterns = [
    path('upload/<str:filename>/', CargarArchivoView.as_view(), name="upload"),
]