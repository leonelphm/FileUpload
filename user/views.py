"""
FileUpload Demo
"""

# @package user.views
#
# Views control para el flujo de usuario
# @author Ing. Leonel P. Hernandez M.
# @version 1.0.0

from rest_framework import status
from rest_framework.exceptions import ParseError
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Imagen

class CargarArchivoView(APIView):
    """!
    Class para cargar un archivo al modelo de datos de imagen

    @author Leonel P. Hernandez M. (leonelphm at gmail.com)
    @date 01-11-2018
    @version 1.0.0
    """
    parser_classes = (FileUploadParser,)

    def put(self, request, filename, format=None):
        """
        Funcion para actualizar el campo de imagen

        @param filname recibe el nombre de la imagen
        @return objeto actualizado
        """
        file_obj = request.FILES
        if 'file' not in request.data:
            raise ParseError("Empty content")
        try:
            foto = Imagen()
            foto.foto.save(filename, file_obj['file'], save=True)
        except Exception as e:
            print(e)
            raise e
        return Response(status=status.HTTP_201_CREATED)

