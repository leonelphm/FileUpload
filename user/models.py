"""
FileUpload Demo
"""

# @package user.models
#
# Models modelo de datos para el almacenamiento del usuario
# @author Ing. Leonel P. Hernandez M.
# @version 1.0.0

from django.db import models


class Imagen(models.Model):
    """!
    Clase que contiene los datos o atributos de imagen

    @author Ing. Leonel P. Hernandez M. (leonelphm@gmail.com )
    @date 01-11-2018
    @version 1.0.0 
    """

    def get_upload_to(self, filename):
        return "fotos/%s" % (filename)

    foto = models.FileField(upload_to=get_upload_to)

    class Meta:
        verbose_name = 'Imagen'
        verbose_name_plural = 'Imagenes'
     
    def __str__(self):
        """!
        Funcion que muestra el objeto de la imagen

        @return Devuelve la ruta de la foto
        """
        return self.foto